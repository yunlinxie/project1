package edu.sjsu.android.project1yunlinxie;

import java.text.DecimalFormat;

public class MorgageCalculator {

    public  static String calculateMorgage(double principle, double annualRate, int years, boolean taxIncluded) {
        double rate = annualRate / 12.0;
        double month = years * 12.0;
        double tax = taxIncluded==true ? 0.001*principle : 0.0;
        double res;

        // when interest rate = 0
        if (rate == 0.0) {
            res = principle/month + tax;
        } else {
            res = principle * rate / (1 - Math.pow(1 + rate, -month)) + tax;
        }

        // Round to two decimal
        DecimalFormat df = new DecimalFormat("#.00");
        String result = df.format(res);

        return  result;
    }

}
