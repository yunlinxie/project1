package edu.sjsu.android.project1yunlinxie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView rateView, resultView;
    private EditText input;
    private SeekBar rate;
    private RadioButton radio15, radio20;
    private CheckBox tax;
    private Button calculateButton, uninstallButton;

    private int currentMax = 200, currentStep = 1;
    private double currentProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText) findViewById(R.id.input);
        rateView = (TextView) findViewById(R.id.rateView);
        rate = (SeekBar) findViewById(R.id.rate);
        radio15 = findViewById(R.id.radio15);
        radio20 = findViewById(R.id.radio20);
        tax = findViewById(R.id.tax);
        calculateButton = findViewById(R.id.calculate);
        uninstallButton = findViewById(R.id.uninstall);
        resultView = (TextView) findViewById(R.id.resultView);

        rate.setMax(currentMax / currentStep);
        rate.setProgress(100);

        rate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar rate, int progress, boolean fromUser) {
                currentProgress = progress * currentStep;
                rateView.setText("Interest rate: " + currentProgress/10.0 + "%");
            }
            @Override
            public void onStartTrackingTouch(SeekBar rate) {}
            @Override
            public void onStopTrackingTouch(SeekBar rate) {}
        });
    }

    public void calculate(View view) {
        // Get inputting principle string
        String principleStr =input.getText().toString();
        // Input is empty
        if (principleStr.isEmpty()) {
            resultView.setText("Please enter the principle.\nThen Press CALCULATE for monthly payments.");
            return;
        }
        // Input has more than two decimals
        if (principleStr.contains(".")) {
            int decimalLen = principleStr.length() - principleStr.indexOf('.') - 1;
            if (decimalLen > 2) {
                resultView.setText("Please enter a valid number. 2 decimal digits max.\nThen Press CALCULATE for monthly payments.");
                return;
            }
        }

        // Prepare all parameters for calculating monthly payment
        double principle = Double.parseDouble(principleStr);
        double annualRate = (double) (rate.getProgress()/1000.0);
        boolean taxIncluded = tax.isChecked()==true? true : false;
        int years;

        // Tree cases for different radio buttons
        if (radio15.isChecked()) {
            years = 15;
        } else  if (radio20.isChecked()) {
            years = 20;
        } else {
            years = 30;
        }

        // Get result by calling function
        String result = MorgageCalculator.calculateMorgage(principle, annualRate, years, taxIncluded);
        resultView.setText("Monthly payment: $" + result);
        return;
    }

    public void uninstall(View view) {
        Intent delete = new Intent(Intent.ACTION_DELETE,
                Uri.parse("package:" + getPackageName()));
        startActivity(delete);
    }


}



























